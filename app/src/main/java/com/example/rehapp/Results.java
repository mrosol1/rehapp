package com.example.rehapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;

public class Results extends AppCompatActivity {

    private GraphView graph;
    private LineGraphSeries<DataPoint> mSeries;
    private int x;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.results);
        graph = (GraphView) findViewById(R.id.graph);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(300);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setScalable(true);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(1024);
        graph.getViewport().scrollToEnd();
        mSeries = new LineGraphSeries<>();
        graph.addSeries(mSeries);
        x = 0;

        Intent i = getIntent();
        double [] emg = i.getDoubleArrayExtra("EMG");
        for (Double j:emg){
            mSeries.appendData(new DataPoint(x, j), false, 1000000000);
            graph.getViewport().setMinX(x - 200);
            graph.getViewport().setMaxX(x + 100);
            x++;
        }
    }
}
