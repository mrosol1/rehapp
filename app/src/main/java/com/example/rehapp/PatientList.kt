package com.example.rehapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.firebase.database.*

class PatientList : AppCompatActivity() {

    lateinit var newButton: Button
    lateinit var deleteButton: Button
    lateinit var patientListView: ListView
    lateinit var patientList: MutableList<Patient>
    lateinit var ref: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.patientlist_layout)

        patientList = mutableListOf()
        ref = FirebaseDatabase.getInstance().getReference("patients")
        newButton = findViewById(R.id.newButton)
        deleteButton = findViewById(R.id.deleteButton)
        patientListView = findViewById(R.id.patientListView)

        newButton.setOnClickListener {
            showAddDialogue()
        }

        deleteButton.setOnClickListener {
            val intent = Intent(this@PatientList,DeletePatient::class.java)
            ContextCompat.startActivity(this@PatientList, intent, intent.extras)
        }

        updatePatientList()
    }

    private fun updatePatientList() {
        ref.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                Log.d("update patient", "przed dodaniem pacjenta")
                if(p0!!.exists()){
                    patientList.clear()
                    for(h in p0.children){
                        Log.d("update patient", "powinno dodać pacjenta")
                        val patient = h.getValue(Patient::class.java)
                        patientList.add(patient!!)
                    }

                    val adapter = PatientAdapter(this@PatientList,R.layout.patient_layout, patientList)
                    patientListView.adapter = adapter
                }
            }

        })



    }
    /*
    private fun updatePatientList() {
        ref.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0!!.exists()){
                    patientList.clear()
                    for(h in p0.children){
                        d("MB", h.getValue(Patient::class.java).toString() )
                        val patient = h.getValue(Patient::class.java)
                        patientList.add(patient!!)
                    }

                    val adapter = PatientAdapter(this@PatientList,R.layout.patient_layout, patientList)
                    patientListView.adapter = adapter
                }
            }

        })

    } */

    private fun showAddDialogue() {
        //addPatient()

        val builder = AlertDialog.Builder(this@PatientList)

        builder.setTitle("Add new patient")

        val inflater = LayoutInflater.from(this@PatientList)
        val view = inflater.inflate(R.layout.add_layout,null)

        val patientNameText = view.findViewById<EditText>(R.id.patientNameText)
        val patientSurnameText = view.findViewById<EditText>(R.id.patientSurnameText)


        builder.setView(view)

        builder.setPositiveButton("Dodaj pacjenta") { dialog, which ->

            val firstName = patientNameText.text.toString().trim()

            if(firstName.isEmpty()){
                patientNameText.error="Wprowadź imię pacjenta"
                return@setPositiveButton
            }

            val surname = patientSurnameText.text.toString().trim()

            if(surname.isEmpty()){
                patientSurnameText.error="Wprowadź nazwisko pacjenta"
                return@setPositiveButton
            }

            val fullName: String
            fullName = firstName + " " + surname

            val trainingList: MutableList<Training> = mutableListOf()

            val patientId = ref.push().key
            val patient = Patient(patientId.toString(), fullName, trainingList)

            ref.child(patientId.toString()).setValue(patient).addOnCompleteListener {
                Toast.makeText(applicationContext,"Dodano pacjenta", Toast.LENGTH_LONG).show()
            }

        }
        builder.setNegativeButton("Nie") { dialog, which -> }

        val alert = builder.create()
        alert.show()
    }

}