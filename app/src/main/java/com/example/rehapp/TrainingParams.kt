package com.example.rehapp

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.trainingparams_layout.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class TrainingParams : AppCompatActivity() {

    lateinit var helloTextView: TextView
    lateinit var goToTrainingButton: Button
    lateinit var ref: DatabaseReference
    lateinit var trainingList: MutableList<Training>
    lateinit var date: String




    @SuppressLint("SetTextI18n")
    @TargetApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.trainingparams_layout)

        val intent = intent
        val name = intent.getStringExtra("name")
     //   Log.d("MB", name)
        helloTextView=findViewById(R.id.helloTextView)
        helloTextView.text = "Witaj, $name!"
        ref = FirebaseDatabase.getInstance().getReference("patients")
        trainingList = mutableListOf()
        goToTrainingButton = findViewById(R.id.goToTrainingButton)
        val current = LocalDateTime.now()

        val formatter = DateTimeFormatter.ISO_LOCAL_DATE
        date = current.format(formatter).toString()

        goToTrainingButton.setOnClickListener {
            if(checkData()){
               // addTraining(name)
                val intent = Intent(this@TrainingParams, Game::class.java)
                intent.putExtra("name", name)
                intent.putExtra("date", date)
                intent.putExtra("pulse", pulseTextView.text.toString())
                intent.putExtra("sysPress", sysPressTextView.text.toString())
                intent.putExtra("diasPress", diasPressTextView.text.toString())
                intent.putExtra("temp", tempTextView.text.toString())
                ContextCompat.startActivity(this@TrainingParams, intent, intent.extras)
            }
        }





    }

    private fun addTraining(name: String?) {
        ref.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    trainingList.clear()

                    for (h in p0.children) {
                        val patient = h.getValue(Patient::class.java)
                        if (patient!!.name == name) {
                        //    trainingList = patient.trainingList.toMutableList()
                        //    trainingList.add(Training(date,pulseTextView.text.toString().toInt(),sysPressTextView.text.toString().toInt(),diasPressTextView.text.toString().toInt(), tempTextView.text.toString().toFloat(),
                          //      arrayListOf()))
                            break
                        }
                    }
                }

            }
        })
    }

    private fun checkData(): Boolean {

        if(pulseTextView.text.toString().isEmpty()){
            pulseTextView.error = "Podaj swój adres e-mail"
            pulseTextView.requestFocus()
            return false
        }
        else if(sysPressTextView.text.toString().isEmpty()){
            sysPressTextView.error = "Podaj swój adres e-mail"
            sysPressTextView.requestFocus()
            return false
        }
        else if(diasPressText.text.toString().isEmpty()){
            diasPressText.error = "Podaj swój adres e-mail"
            diasPressText.requestFocus()
            return false
        }
        else if(tempTextView.text.toString().isEmpty()){
            tempTextView.error = "Podaj swój adres e-mail"
            tempTextView.requestFocus()
            return false
        }
        else if(pulseTextView.text.toString().toInt()<0 ||sysPressTextView.text.toString().toInt()<0 ||diasPressTextView.text.toString().toInt()<0 ||tempTextView.text.toString().toFloat()<0){
            Toast.makeText(baseContext, "Podane parametry są błędne. Wprowadź ponownie.",
                Toast.LENGTH_SHORT).show()
            return false
        }
        else {
            return true
        }
    }
}
