package com.example.rehapp;
import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.google.firebase.database.*;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Game extends BlunoLibrary implements AdapterView.OnItemSelectedListener {
    private Button buttonStart, buttonTrening, buttonKal;
    private ImageView dioda, man, przeszkoda, tlo;
    private GraphView graph;
    private LineGraphSeries<DataPoint> mSeries;
    private int x, previousSelected, margin;
    private Spinner spinner;
    private String sygnal, connected;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 456;
    private static final int PERMISSION_REQUEST_CALENDAR = 456;
    private List<Double> dane, allData;
    private RelativeLayout.LayoutParams mParams1;
    private Date currentTime;
    private DatabaseReference ref;
    private String name,date, pulse,sysPres,diasPres,temp;
    private List<Training> trList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);
        onCreateProcess();                                                        //onCreate Process by BlunoLibrary
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        }

       // przyjęcie imienia z ekranu logowania
        Intent i = getIntent();
        name = (String) i.getStringExtra("name"); // Pobranie danych pacjenta, żeby potem go znaleźć w bazie danych i zapisać wynik ćwiczeń
        temp = (String) i.getStringExtra("temp");
        date = (String) i.getStringExtra("date");
        pulse = (String) i.getStringExtra("pulse");
        sysPres = (String) i.getStringExtra("sysPress");
        diasPres = (String) i.getStringExtra("diasPress");

        man = (ImageView) findViewById(R.id.Man);
        man.setImageResource(R.mipmap.man);

        przeszkoda = (ImageView) findViewById(R.id.Przeszkoda);
        przeszkoda.setImageResource(R.color.colorAccent);

        tlo = (ImageView) findViewById(R.id.Tlo);
        tlo.setImageResource(R.color.colorPrimary);

        dane = new ArrayList<>();
        allData = new ArrayList<>();

        x = 0;
        sygnal = "0";
        connected = "disconnected";
        previousSelected = 0;
        serialBegin(115200);                                                    //set the Uart Baudrate on BLE chip to 115200
        // ScanProcess();
        dioda = (ImageView) findViewById(R.id.dioda);
        dioda.setImageResource(R.mipmap.red);
        buttonStart = (Button) findViewById(R.id.buttonSTART);
        buttonStart.setEnabled(false);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("on click", "kliłem");
                if (buttonStart.getText().equals("START")) {
                    Log.d("on click", "kliłem START");
                    buttonStart.setText("STOP");
                    graph.removeAllSeries();
                    mSeries = new LineGraphSeries<>();
                    graph.addSeries(mSeries);
                    x = 0;


                } else {
                    Log.d("on click", "kliłem STOP");
                    buttonStart.setText("START");

                    addTraining(name);


                }
            }
        });
        graph = (GraphView) findViewById(R.id.graph);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(300);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setScalable(true);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(1024);
        graph.getViewport().scrollToEnd();
        mSeries = new LineGraphSeries<>();
        graph.addSeries(mSeries);

        ref = FirebaseDatabase.getInstance().getReference("patients");

        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.Options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        spinner.setSelection(0);

        mParams1 = (RelativeLayout.LayoutParams) przeszkoda.getLayoutParams();
        margin = mParams1.leftMargin;

    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        if (pos == 0) { //Game
            Log.d("select item", "wybrano 0 chuj");
            if (previousSelected != 3 && connected.equals("connected"))
                serialSend("1");

            graph.setVisibility(View.INVISIBLE);
            man.setVisibility(View.VISIBLE);
            przeszkoda.setVisibility(View.VISIBLE);
            tlo.setVisibility(View.VISIBLE);

            previousSelected = 0;
            sygnal = "0";
            graph.removeAllSeries();
            mSeries = new LineGraphSeries<>();
            graph.addSeries(mSeries);
            x = 0;
        }
        if (pos == 1) { // Raw
            graph.getViewport().setMaxY(250);
            if (previousSelected != 3 && connected.equals("connected"))
                serialSend("0");
            sygnal = "1";
            previousSelected = 1;
            graph.getViewport().setMaxY(1024);
            graph.removeAllSeries();
            mSeries = new LineGraphSeries<>();
            graph.addSeries(mSeries);
            x = 0;
            graph.setVisibility(View.VISIBLE);
            man.setVisibility(View.INVISIBLE);
            przeszkoda.setVisibility(View.INVISIBLE);
            tlo.setVisibility(View.INVISIBLE);
        }
        if (pos == 2) { //RMS
            graph.getViewport().setMaxY(250);
            if (previousSelected != 2 && connected.equals("connected"))
                serialSend("1");
            sygnal = "1";
            previousSelected = 2;
            graph.removeAllSeries();
            mSeries = new LineGraphSeries<>();
            graph.addSeries(mSeries);
            x = 0;
            graph.setVisibility(View.VISIBLE);
            man.setVisibility(View.INVISIBLE);
            przeszkoda.setVisibility(View.INVISIBLE);
            tlo.setVisibility(View.INVISIBLE);
        }
        if (pos == 3) {
            if (connected.equals("disconnected")) {
                scanLeDevice(true);
                spinner.setSelection(previousSelected);
            }
            spinner.setSelection(previousSelected);
            previousSelected = 3;
        }

    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    protected void onResume() {
        super.onResume();
        System.out.println("BlUNOActivity onResume");
        onResumeProcess();                                                        //onResume Process by BlunoLibrary
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        onActivityResultProcess(requestCode, resultCode, data);                    //onActivityResult Process by BlunoLibrary
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        connected = "disconnected";
        dioda.setImageResource(R.mipmap.red);
        onPauseProcess();                                                        //onPause Process by BlunoLibrary
    }

    protected void onStop() {
        super.onStop();
        connected = "disconnected";
        dioda.setImageResource(R.mipmap.red);
        onStopProcess();                                                        //onStop Process by BlunoLibrary
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        connected = "disconnected";
        dioda.setImageResource(R.mipmap.red);
        onDestroyProcess();                                                        //onDestroy Process by BlunoLibrary
    }

    @Override
    public void onConectionStateChange(connectionStateEnum theConnectionState) {//Once connection state changes, this function will be called
        switch (theConnectionState) {                                            //Four connection state
            case isConnected:
             //   if (previousSelected == 1)
               //     serialSend("1");
                //if (previousSelected == 0)
                 //   serialSend("0");
                buttonStart.setText("START");
                dioda.setImageResource(R.mipmap.green);
                connected = "connected";
                buttonStart.setEnabled(true);
                break;
            case isConnecting:
                buttonStart.setText("...");
                dioda.setImageResource(R.mipmap.blue);
                break;
            case isToScan:
                buttonStart.setText("START");
                dioda.setImageResource(R.mipmap.red);
                connected = "disconnected";
                break;
            case isScanning:
                buttonStart.setText("...");
                dioda.setImageResource(R.mipmap.red);
                connected = "disconnected";
                break;
            case isDisconnecting:
                buttonStart.setText("...");
                dioda.setImageResource(R.mipmap.red);
                connected = "disconnected";
                break;
            default:
                break;
        }
    }

    @Override
    public void onSerialReceived(String theString) {

        String lines[] = theString.split("\\r?\\n");
        if (buttonStart.getText().equals("STOP")) {
            for (int i = 0; i < lines.length; i++) {
                if (lines.length <= 2 || i != 0 && i != lines.length - 1) {
                    try {
                        mSeries.appendData(new DataPoint(x, Double.parseDouble(lines[i])), false, 1000000000);
                        graph.getViewport().setMinX(x - 200);
                        graph.getViewport().setMaxX(x + 100);

                        double skala = tlo.getHeight() / 520.0;
                        Log.d("skala: ", String.valueOf(skala));
                        Log.d("wartość: ", String.valueOf(Double.parseDouble(lines[i]) * skala));

                        dane.add(Double.parseDouble(lines[i]) * skala);
                        allData.add(Double.parseDouble(lines[i]));
                        if (x > 7) {
                            dane.remove(1);
                        }

                        RelativeLayout.LayoutParams mParams = (RelativeLayout.LayoutParams) man.getLayoutParams();
                        mParams.bottomMargin = (int) calculateAverage(dane);
                        man.setLayoutParams(mParams);
                        Log.d("bottom margin: ", String.valueOf(mParams.bottomMargin));


                        RelativeLayout.LayoutParams mParams1 = (RelativeLayout.LayoutParams) przeszkoda.getLayoutParams();
                        mParams1.rightMargin = mParams1.rightMargin + 5;
                        mParams1.leftMargin = mParams1.leftMargin - 5;
                        przeszkoda.setLayoutParams(mParams1);
                        if (mParams1.leftMargin <= -100) {
                            mParams1.rightMargin = 10;
                            mParams1.leftMargin = margin;
                            przeszkoda.setLayoutParams(mParams1);
                        }


                        x++;

                        System.out.println(lines[i]);
                    } catch (NumberFormatException e) {
                        Log.d("niewłaściwy format : ", lines[i]);
                    }
                }
            }

            Log.d("to jest jeden zestaw", theString);
            //   System.out.println(theString);

        }
    }

    private double calculateAverage(List<Double> lista) {
        Double sum = 0.0;
        if (!lista.isEmpty()) {
            for (Double m : lista) {
                sum += m;
            }
            return sum / lista.size();
        }
        return sum;
    }

    private void addTraining(final String name) {

        ref.addValueEventListener(new ValueEventListener() {
            boolean a = true;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (a) {
                    if (dataSnapshot.exists()) {
                        // trainingList.clear()
                        System.out.println(dataSnapshot.getValue());
                    }

                    outerloop:
                    for (DataSnapshot h : dataSnapshot.getChildren()) {
                        Patient patient = h.getValue(Patient.class);
                        Log.d("Imie akt pacjenta", patient.getName());
                        Log.d("Imie pacjenta", name);

                        if (patient.getName().equals(name)) {
                            ArrayList<Training> treList;
                            if (patient.getTrainingList().isEmpty())
                                treList = new ArrayList<>();
                            else
                                treList = (ArrayList<Training>) patient.getTrainingList();
                            Training t = new Training(date, pulse, sysPres, diasPres, temp, allData);
                            Log.d("Trening", t.toString());
                            treList.add(t);
                            Log.d("Loop1", "Powiedz, powiedz czmeu?");
                            Patient p = new Patient(patient.getId(), patient.getName(), treList);
                            ref.child(patient.getId()).setValue(p);
                            a = false;
                            Log.d("Loop1", "set value child");
                            Toast.makeText(getApplicationContext(), "Dodano trening", Toast.LENGTH_LONG).show();
                            Intent in = new Intent(Game.this, MainActivity.class);
                            in.putExtra("name", name);
                            Log.d("Loop1", "Intent");
                            startActivity(in);
                            Log.d("Loop1", "stary intent");
                            break outerloop;
                        }


                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }
}