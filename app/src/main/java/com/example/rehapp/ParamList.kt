package com.example.rehapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

class ParamList : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.param_layout)

        var intent = intent
        val pulse = intent.getStringExtra("pulse")
        val sysPressure = intent.getStringExtra("sysPressure")
        val diasPressure = intent.getStringExtra("diasPressure")
        val temperature = intent.getStringExtra("temperature")
        val emg:DoubleArray = intent.getDoubleArrayExtra("EMG")

        val pulseTextView =findViewById<TextView>(R.id.pulseTextView)
        val sysPressTextView =findViewById<TextView>(R.id.sysPressTextView)
        val diasPressTextView =findViewById<TextView>(R.id.diasPressTextView)
        val tempTextView =findViewById<TextView>(R.id.tempTextView)
        val showButton = findViewById<Button>(R.id.showButton)
        showButton.setOnClickListener(){
            val intent = Intent(this@ParamList, Results::class.java)
            intent.putExtra("EMG", emg)
            ContextCompat.startActivity(this@ParamList, intent, intent.extras)
        }


        pulseTextView.text = pulse
        sysPressTextView.text = sysPressure
        diasPressTextView.text = diasPressure
        tempTextView.text = temperature
    }
}
