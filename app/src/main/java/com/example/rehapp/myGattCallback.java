package com.example.rehapp;

import android.app.Service;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.util.List;
import java.util.UUID;

public class myGattCallback extends BluetoothGattCallback implements Handler.Callback {
    public static final int MSG_DISCOVER_SERVICE = 10;
    private Handler bleHander;
    private UUID serviceUUID = UUID.fromString("0000dfb1-0000-1000-8000-00805f9b34fb");
    public myGattCallback(){
        HandlerThread handlerThread = new HandlerThread("BLE-Worker");
        handlerThread.start();
        bleHander = new Handler(handlerThread.getLooper(),this);
    }

    public void dispose(){
        bleHander.removeCallbacksAndMessages(null);
        bleHander.getLooper().quit();
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState){
        super.onConnectionStateChange(gatt,status,newState);
        Log.d("btCallback","onConnectionStateChange");
        Log.d("btCallback",gatt.toString());
        Log.d("btCallback",Integer.toString(newState));
        Log.d("btCallback",Integer.toString(status));
        if(newState == BluetoothProfile.STATE_CONNECTED){
            bleHander.obtainMessage(MSG_DISCOVER_SERVICE,gatt).sendToTarget();
        }
        if(newState == BluetoothProfile.STATE_DISCONNECTED){
            Log.d("btCallback", "rozłączono :((((");
            gatt.close();
        }
        if(newState == BluetoothProfile.STATE_CONNECTED){
            Log.d("btCallback", "łączenieeee");
        }
        if(newState == BluetoothProfile.STATE_DISCONNECTING){
            Log.d("btCallback", "rozłączanieeeee");
        }
    }



    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status){
        Log.d("btCallback", "onServicesDiscovered");
        super.onServicesDiscovered(gatt, status);
        bleHander.obtainMessage(MSG_DISCOVER_SERVICE,gatt).sendToTarget();
        if(status == BluetoothGatt.GATT_SUCCESS){
            BluetoothGattService mBluetoothGattService = gatt.getService(serviceUUID);
            if (mBluetoothGattService != null) {
                Log.i("pobieramy servie", "kurwa: " + mBluetoothGattService.getUuid().toString());
            } else {
                Log.i("pobieramy service", "Service characteristic not found for UUID: " + serviceUUID.toString());
            }

        }

    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic c, int ststus){
        if( ststus == BluetoothGatt.GATT_SUCCESS){
            Log.d("UUID",c.getUuid().toString());
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch(msg.what){
            case MSG_DISCOVER_SERVICE:
                BluetoothGatt gatt = (BluetoothGatt) msg.obj;
                gatt.discoverServices();
                Log.d("btCallback", "discover Services in hanlde Message");
                break;
        }
        return true;
    }

}
